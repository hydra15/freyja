import 'dart:async';
import 'package:flutter/material.dart';
import 'package:freyja/main.dart';

import 'package:animated_text_kit/animated_text_kit.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({
    super.key,
    required this.title,
    required this.description,
    required this.image,
  });

  final String title;
  final String description;
  final String image;
  final int gleep = 5;

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: widget.gleep), () => Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (BuildContext context) => const MyHomePage(title: 'default_title')
      )
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const SizedBox(width: 20.0, height: 100.0),
            Image.asset(widget.image),

            // AnimatedTextKit(
            //   animatedTexts: [
            //     TypewriterAnimatedText(
            //       '${widget.title}|${widget.description}',	
            //       textStyle: const TextStyle(
            //         fontSize: 25.0,
            //         fontWeight: FontWeight.bold,
            //         color: Colors.red,
            //       ),
            //       speed: const Duration(milliseconds: 200),
            //     ),
            //   ],
            //   totalRepeatCount: 1,
            //   pause: const Duration(milliseconds: 500),
            //   displayFullTextOnTap: true,
            //   stopPauseOnTap: true,
            // ),

            const SizedBox(width: 20.0, height: 100.0),
            DefaultTextStyle(
              style: TextStyle(
                fontSize: 40.0,
                fontFamily: 'Horizon',
                color: Colors.red.shade900,
              ),
              child: AnimatedTextKit(
                animatedTexts: [
                  RotateAnimatedText('Welcome!!', duration: const Duration(milliseconds: 1000)),
                  RotateAnimatedText('Willkommen!!', duration: const Duration(milliseconds: 1000)),
                  RotateAnimatedText('Bienvenido!!', duration: const Duration(milliseconds: 1000)),
                ],
                pause: const Duration(milliseconds: 500),
              ),
            ),
          ],
        // ),
      // ),
    );
  }
}
